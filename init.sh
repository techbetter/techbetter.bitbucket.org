#!/bin/bash

## Initialization script

set -e

export HOSTNAME=`hostname`

## We are assuming that "noc" is a Salt Master.
if [[ "${HOSTNAME}" =~ "noc" ]]; then
  export ENABLE_SALT_MASTER=1
fi

if [ -f "/etc/lsb-release" ]; then
  source /etc/lsb-release
  if [[ "trusty" != "${DISTRIB_CODENAME}" ]]; then
    echo "Unsupported Debian/Ubuntu distribution. This script is designed to work only on Ubuntu 14.04 LTS."
    exit 1
  fi
else
  echo "This script is designed to work only on Ubuntu 14.04 LTS."
  exit 2
fi

if [[ ${RUNAS} ]]; then
    export run_as="$RUNAS"
else
    read -p "Run salt-call as which user?: " run_as
fi

if [[ ${INIT_LEVEL} ]]; then
    export level="$INIT_LEVEL"
else
    while true; do
        read -p "What is this deployment targeted level? [sandbox,test,staging,production,vagrant]: " level
        case $level in
            sandbox ) break;;
            test ) break;;
            staging ) break;;
            production ) break;;
            vagrant ) break;;
            * ) echo "Only lowercase is accepted; one of [sandbox,test,staging,production,vagrant].";;
        esac
    done
fi

## What we know we'll need in our states for certain
## - timelib: Because we have states that generates timestamps
## - pygit2: Because we leverage GitFS with Salt post version 2005
apt-get install -y git git-core python-software-properties

if [[ ! -f /etc/apt/sources.list.d/ppa-rhansen-pygit2.list ]]; then
  apt-key adv --keyserver keyserver.ubuntu.com --recv ACABB5F5
  echo 'deb http://ppa.launchpad.net/rhansen/pygit2/ubuntu trusty main' > /etc/apt/sources.list.d/ppa-rhansen-pygit2.list

  ## Above would be equivalent to below. We want this to exist even before salt is installed.
  #deb http://ppa.launchpad.net/rhansen/pygit2/ubuntu trusty main:
  #  pkgrepo.managed:
  #    - keyserver:   hkp://keyserver.ubuntu.com:80
  #    - keyid:       ACABB5F5
  #    - refresh_db:  True

  ## Thanks https://repo.saltstack.com/#ubuntu
  wget -O - https://repo.saltstack.com/apt/ubuntu/14.04/amd64/2016.3/SALTSTACK-GPG-KEY.pub | sudo apt-key add -
  echo 'deb http://repo.saltstack.com/apt/ubuntu/14.04/amd64/2016.3 trusty main' > /etc/apt/sources.list.d/saltstack-salt-trusty.list

  apt-get update
fi

apt-get install -y salt-minion python-pygit2 python-timelib

if [[ ${ENABLE_SALT_MASTER} ]]; then
  salt-call --local hosts.set_host 127.0.1.1 "noc.${level}.techbetter.com noc"
fi

mkdir -p /etc/salt/minion.d
[[ ! -f "/etc/salt/minion.d/id.conf" ]] && printf "id: ${HOSTNAME}\n" > /etc/salt/minion.d/id.conf
[[ ! -f "/etc/salt/minion.d/master.conf" ]] && printf "master: noc.${level}.techbetter.com\n" > /etc/salt/minion.d/master.conf

(cat <<- __END_EMBEDDED_FILE__
hash_type: sha256
__END_EMBEDDED_FILE__
) > /etc/salt/minion.d/common.conf

salt-call --local grains.set level ${level}

if [[ ${ENABLE_SALT_MASTER} ]]; then
  service salt-minion restart
  echo 'This will be a salt-master'
  apt-get install -y salt-master
  [[ ! -L "/etc/salt/master.d/common.conf" ]] && ln -s /etc/salt/minion.d/common.conf /etc/salt/master.d/common.conf
  echo 'Auto-accepting this node'
  COUNTER=0
  WAIT=5
  MAX=6
  until salt-key -y -A -q
  do
    let COUNTER+=1
    if [[ ${COUNTER} -ge ${MAX} ]]; then
      echo "Reached ${MAX}. You may have to accept manually by issuing 'salt-key -y -a ${HOSTNAME}' yourself"
      break
    fi
    echo "Failed ${COUNTER} time(s), we will wait ${WAIT} seconds until ${MAX} attempts are made"
    sleep ${WAIT}
  done
  #salt-call --local saltutil.wheel key.accept ${HOSTNAME}
else
  echo 'This will be a minion'
  service salt-minion restart
fi
