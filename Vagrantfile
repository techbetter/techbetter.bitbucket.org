# Define VM memory usage through environment variables
MEMORY = ENV.fetch("VAGRANT_MEMORY", "2048")
PROVIDER = ENV.fetch("VAGRANT_DEFAULT_PROVIDER", "virtualbox")

Vagrant.configure(2) do |config|
  config.vm.hostname = "web-dev"

  config.vm.box = 'trusty-cloud'
  config.vm.box_url = 'https://cloud-images.ubuntu.com/vagrant/trusty/current/trusty-server-cloudimg-amd64-vagrant-disk1.box'
  config.ssh.forward_agent = true

  # ref: https://github.com/mitchellh/vagrant/issues/1673
  config.vm.provision "fix-no-tty", type: "shell" do |s|
      s.privileged = false
      s.inline = "sudo sed -i '/tty/!s/mesg n/tty -s \\&\\& mesg n/' /root/.profile"
  end

  config.vm.network "private_network", type: "dhcp"

  if Vagrant.has_plugin?("vagrant-cachier")
    # Configure cached packages to be shared between instances of the same base box.
    # More info on http://fgrehm.viewdocs.io/vagrant-cachier/usage
    config.cache.scope = :box
    config.cache.enable :apt
  end

  config.vm.synced_folder "www", "/srv/www", create: true
  config.vm.synced_folder "provision/certificates", "/srv/salt/files/openssl/certificates", create: true
  config.vm.synced_folder "provision/npm_packages", "/srv/webapps/npm_packages", create: true
  config.vm.synced_folder "log_php", "/var/log/php", create: true

  if PROVIDER.match("virtualbox")
    config.vm.provider "virtualbox" do |v|
      v.customize ["modifyvm", :id, "--memory", MEMORY]
      v.customize ["modifyvm", :id, "--description", "Vagrant Web Development workbench located in " + File.dirname(__FILE__) ]
      v.customize ['modifyvm', :id, '--ostype', 'Ubuntu_64']
      v.customize ["modifyvm", :id, "--pae", "on"]
    end
  end

  config.vm.provision "shell", run: "always", inline: <<-SHELL
#!/bin/bash

##
## Standalone and masterless Web Development Server
##

set -e

if [[ ! -f /vagrant/.ssh/id_rsa ]]; then
  mkdir -p /vagrant/.ssh
  ssh-keygen -q -N '' -f /vagrant/.ssh/id_rsa
fi
echo '--------------------------------------------------------------------'
echo 'IMPORTANT: Ensure you have the following in your Bitbucket SSH keys!'
cat /vagrant/.ssh/id_rsa.pub
echo '--------------------------------------------------------------------'
echo '... waiting 5 seconds'
sleep 5

if [[ ! -f /etc/salt/minion.d/fileserver.conf ]]; then
  export INIT_LEVEL="vagrant"
  export RUNAS="vagrant"
  curl -s -S -L "http://aliaswebservices.bitbucket.org/init.sh" | bash
  salt-call --local ssh.set_known_host user=root hostname=github.com
  salt-call --local ssh.set_known_host user=root hostname=bitbucket.org
fi
if [[ -f /etc/salt/minion.d/master.conf ]]; then
  rm -rf /etc/salt/minion.d/master.conf
fi

## Want to use NOC workspace already cloned in your workstation
## Uncomment lines below. You may need to adjust add_host IP though.
#salt-call --local ssh.set_known_host user=root hostname=workstation
#salt-call --local hosts.add_host 172.28.128.1 workstation

(cat <<- __END_EMBEDDED_FILE__
# Development TLS certificates

The files in this directory are managed via Vagrant.

Changing their contents, may lead you to having problems accessing development
sites over TLS (HTTPS).

Erasing them may require you having to remove self signed certificates
from your Web Browser.

__END_EMBEDDED_FILE__
) > /srv/salt/files/openssl/certificates/README.md

## Make sure this matches
## https://bitbucket.org/AliasWebServices/noc/src
## what what's in ops/provision/cloud.
## Better document difference, one settled #TODO
(cat <<- __END_EMBEDDED_FILE__
## Written at first boot time via Vagrantfile
fileserver_backend:
  - roots
  - git

gitfs_provider: pygit2
gitfs_base: master
gitfs_env_whitelist:
  - base
gitfs_privkey: /vagrant/.ssh/id_rsa
gitfs_pubkey: /vagrant/.ssh/id_rsa.pub
gitfs_remotes:
  ## Want to use NOC workspace already cloned in your workstation
  ## Adjust lines below;
  #- renoirb@workstation:/Users/renoirb/workspaces/betastream/noc/ops/configuration/states
  #- renoirb@workstation:/Users/renoirb/workspaces/betastream/noc/ops/configuration/formulas/basesystem
  - git@bitbucket.org:AliasWebServices/salt-states.git
  - git@bitbucket.org:AliasWebServices/salt-basesystem.git

git_pillar_provider: pygit2
ext_pillar:
  - git:
    ## Want to use NOC workspace already cloned in your workstation
    ## Adjust and replace line below;
    #- master renoirb@workstation:/Users/renoirb/workspaces/betastream/noc/ops/configuration/pillars:
    - master git@bitbucket.org:AliasWebServices/pillars.git:
      - privkey: /vagrant/.ssh/id_rsa
      - pubkey: /vagrant/.ssh/id_rsa.pub
  - cmd_yaml: cat /vagrant/provision/pillar.yml

__END_EMBEDDED_FILE__
) > /etc/salt/minion.d/fileserver.conf
## What's between __END_EMBEDDED_FILE__ MUST be at column 0

(cat <<- __END_EMBEDDED_FILE__
## Written at first boot time via Vagrantfile
file_client: local
__END_EMBEDDED_FILE__
) > /etc/salt/minion.d/standalone.conf
## What's between __END_EMBEDDED_FILE__ MUST be at column 0

if [[ ! -f /vagrant/provision/pillar.yml ]]; then
  echo 'Making sure we have a /vagrant/provision/pillar.yml'
  mkdir -p /vagrant/provision
  touch /vagrant/provision/pillar.yml
fi

## First time we do not find the file, just do it
if [[ ! -f /root/.vagrant_do_apt_update ]]; then
  apt-get update &&\
  apt-get -y upgrade &&\
  apt-get -y dist-upgrade &&\
  touch /root/.vagrant_do_apt_update
fi

## Then, next bootups, conditionally update system
if find /root/.vagrant_do_apt_update -mtime +4 | grep . > /dev/null 2>&1
then
  echo 'We will be upgrading packages'
  apt-get update &&\
  apt-get -y upgrade &&\
  apt-get -y dist-upgrade &&\
  touch /root/.vagrant_do_apt_update
else
  echo 'We will not be upgrading packages today, we did it recently already.'
fi

service salt-minion restart

echo 'Running apply, this may take a while.'

salt-call state.apply -l info

if [[ -f /usr/bin/salt-call ]]; then
  salt-call --local --log-level=quiet --no-color grains.get ip4_interfaces:eth1 --output=json | python -c 'import sys,json; print json.load(sys.stdin)["local"][0]' > /vagrant/.ip
  IP=`cat /vagrant/.ip`
  echo "All done!"
  echo "Now, you can do the following:"
  echo "  Use the VM with this IP address: ${IP}"
  echo "  Go to http://${IP}/adminer to manage your local databases"
  echo "  Ensure you have the following to your Workstation's /etc/hosts file (or C:/Windows/System32/drivers/etc/hosts)"
  echo '--------------------------------------------------------------------'
  ls -1 /srv/www | sed 's/$/.vagrant.alias.services/' | sed "s/^/${IP}      /"
  echo '--------------------------------------------------------------------'
fi

if [[ -d "/srv/webapps" ]]; then
  if [[ ! -d "/srv/webapps/.ssh" ]]; then
    echo 'Folder /srv/webapps/.ssh/ did not exist, lets create and copy your SSH key to it'
    mkdir -p /srv/webapps/.ssh
  else
    echo 'Folder /srv/webapps/.ssh/ existed, lets make sure it has the same SSH key as /vagrant/.ssh'
  fi
  cp /vagrant/.ssh/id_rsa{,.pub} /srv/webapps/.ssh
  chown webapps:webapps /srv/webapps/.ssh/id_rsa{,.pub}
else
  echo 'Folder /srv/webapps/ did not exist, at this stage it SHOULD. Please send me an email at hello@renoirboulanger.com about this. Thanks!'
  exit 1
fi

if [[ -d "/home/vagrant/.ssh" ]] && [[ -f "/home/vagrant/.ssh/authorized_keys" ]]; then
    echo 'Change authorized_keys to 600 to fix a bug in vagrant 1.8.5 that asks for a password on vagrant ssh.'
    chmod 600 /home/vagrant/.ssh/authorized_keys
fi

  SHELL
end
